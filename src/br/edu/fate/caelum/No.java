package br.edu.fate.caelum;
public class No {
	private No anterior;
	public No getAnterior() {
		return anterior;
	}

	public void setAnterior(No anterior) {
		this.anterior = anterior;
	}

	public void setElemento(Object elemento) {
		this.elemento = elemento;
	}

	private No proxima;
	private Object elemento;

	public No(No proxima, Object elemento) {
		this.proxima = proxima;
		this.elemento = elemento;
	}

	public No(Object elemento) {
		this.elemento = elemento;
	}

	public void setProxima(No proxima) {
		this.proxima = proxima;
	}

	public No getProxima() {
		return proxima;
	}

	public Object getElemento() {
		return elemento;
	}
	
	
}