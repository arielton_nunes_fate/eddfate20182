package br.edu.fate.listas;

public class Elemento {
	int informacao;
	No proximo;
	
	public Elemento() {
		proximo = new No();
	}
	
	public int getInformacao() {
		return informacao;
	}
	public void setInformacao(int informacao) {
		this.informacao = informacao;
	}
	public No getProximo() {
		return proximo;
	}
	public void setProximo(No proximo) {
		this.proximo = proximo;
	}
	public boolean hasNext() {
		return proximo.getElemento() != null;
	}
}
