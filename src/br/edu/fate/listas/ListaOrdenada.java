package br.edu.fate.listas;

public class ListaOrdenada extends Lista {

	public void insereNoFim(int valor) {
		if (this.primeiro == null) {
			insere(valor);
		} else {
			Elemento novo = new Elemento();
			novo.setInformacao(valor);

			Elemento elementoLista = this.primeiro;
			while (elementoLista.hasNext()) {
				elementoLista = elementoLista.proximo.getElemento();
			}
			elementoLista.proximo.setElemento(novo);
		}

	}

	public void insereOrdenado(int valor) {

		ListaOrdenada nova = new ListaOrdenada();

		if (this.primeiro == null) {
			nova.insere(valor);
		} else {
			Elemento novo = new Elemento();
			novo.setInformacao(valor);

			Elemento elementoLista = this.primeiro;
			boolean inserido = false;
			if (valor < elementoLista.informacao) {
				nova.insere(valor);
				nova.insereNoFim(elementoLista.informacao);
				inserido = true;
			} else {
				nova.insereNoFim(elementoLista.informacao);
			}
			while (elementoLista.hasNext()) {
				int anterior = elementoLista.informacao;
				elementoLista = elementoLista.proximo.getElemento();
				if (!inserido && ((anterior <= valor) && (valor < elementoLista.getInformacao()))) {
					nova.insereNoFim(valor);
					nova.insereNoFim(elementoLista.informacao);
					inserido = true;
				} else {
					nova.insereNoFim(elementoLista.informacao);
				}
			}
			if (!inserido) {
				nova.insereNoFim(valor);
				inserido = true;
			}
		}
		this.primeiro = nova.primeiro;
	}

}
