package br.edu.fate.listas;

public class ListaEncadeada extends Lista{
	
	
	public void insereNoFim(int valor) {
		if (this.primeiro == null) {
			insere(valor);
		}
		else {
			Elemento novo = new Elemento();
			novo.setInformacao(valor);
			
			Elemento elementoLista = this.primeiro;
			while(elementoLista.hasNext()) {
				elementoLista = elementoLista.proximo.getElemento();
			}
			elementoLista.proximo.setElemento(novo);
		}
			
	}
	
}
