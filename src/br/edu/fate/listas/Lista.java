package br.edu.fate.listas;

public class Lista {
	Elemento primeiro;

	public Lista() {
	}
	
	public Elemento getPrimeiro() {
		return primeiro;
	}

	public void setPrimeiro(Elemento primeiro) {
		this.primeiro = primeiro;
	}
	
	public void insere(int valor) {
		this.primeiro = new Elemento();
		this.primeiro.setInformacao(valor);
	}
}
