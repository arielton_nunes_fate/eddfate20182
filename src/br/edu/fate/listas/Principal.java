package br.edu.fate.listas;

public class Principal {

	public static void main(String[] args) {
		ListaOrdenada listaOrdenada = new ListaOrdenada();
		listaOrdenada.insereOrdenado(8);
		listaOrdenada.insereOrdenado(7);
		listaOrdenada.insereOrdenado(6);
		listaOrdenada.insereOrdenado(9);
		listaOrdenada.insereOrdenado(1);
		listaOrdenada.insereOrdenado(2);
		listaOrdenada.insereOrdenado(5);
		listaOrdenada.insereOrdenado(17);
		listaOrdenada.insereOrdenado(18);
		listaOrdenada.insereOrdenado(70);
		listaOrdenada.insereOrdenado(45);
		listaOrdenada.insereOrdenado(33);
		listaOrdenada.insereOrdenado(50);
		listaOrdenada.insereOrdenado(20);
		listaOrdenada.insereOrdenado(3);
		listaOrdenada.insereOrdenado(4);
		
		Elemento e = listaOrdenada.getPrimeiro();
		System.out.println(e.getInformacao());
		while (e.hasNext()) {
			e = e.getProximo().getElemento();
			System.out.println(e.getInformacao());
		}
		
	}
	
}
